FROM pypy:3

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY main.py ./main.py

ENV DB_USER=1
ENV DB_PASSWORD=1
ENV DB_HOST=1
ENV DB_PORT=1
ENV DB_DB=1
ENV URL=1
ENV TG_TOKEN=1
ENV ALLOWED_USERS=1

CMD [ "pypy3", "./main.py" ]