import os
import re

import sqlalchemy as db
from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext


def start(update: Update, context: CallbackContext) -> None:
    update.message.reply_html(f'Hi <code>{update.effective_user.id}</code>!')


def echo(update: Update, context: CallbackContext) -> None:
    if str(update.effective_user.id) in allowed_users:
        if re.match(r"[^@]+@[^@]+\.[^@]+", update.message.text):
            result = getUser(update.message.text)
            if result[0]:
                update.message.reply_html(f'Done \n<code>https://{url}/ghost/#/signup/{result[1]}</code>')
            else:
                update.message.reply_html('INVALID EMAIL')
        else:
            return
    else:
        return


def getUser(email: str):
    invites = db.Table('invites', db.MetaData(), autoload=True, autoload_with=engine)
    query = db.select([invites]).where(invites.columns.email == email)
    result_proxy = connection.execute(query)
    result = result_proxy.fetchall()
    if len(result) == 0:
        return [False, ""]
    else:
        query = db.update(invites).values(status="sent").where(invites.columns.id == result[0][0])
        connection.execute(query)
        return [True, result[0][3]]


def telegram_bot(token: str):
    updater = Updater(token)
    dispatcher = updater.dispatcher

    dispatcher.add_handler(CommandHandler("start", start))

    # on non command i.e message - echo the message on Telegram
    dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, echo))

    # Run the bot until the user presses Ctrl-C
    updater.start_polling()

    updater.idle()


if __name__ == '__main__':
    user = os.getenv("DB_USER")
    password = os.getenv("DB_PASSWORD")
    host = os.getenv("DB_HOST")
    port = os.getenv("DB_PORT")
    database = os.getenv("DB_DB")
    url = os.getenv("URL")
    token = os.getenv("TG_TOKEN")
    allowed_users = os.getenv("ALLOWED_USERS")
    if user is None \
            or password is None \
            or host is None \
            or port is None \
            or database is None \
            or token is None \
            or url is None:
        print("NEED ENV")
        exit()

    allowed_users = allowed_users.split(",")
    engine = db.create_engine(f'mysql+pymysql://{user}:{password}@{host}:{port}/{database}')
    connection = engine.connect()
    telegram_bot(token)

